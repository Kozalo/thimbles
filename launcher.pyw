#!/usr/bin/python
# -*- coding: utf-8 -*-

"""A main executable module of the game."""

# IMPORTS
import sys
import random
import pygame
from pygame.locals import *
from viscera import *


# CONSTANTS
cWINS, cLOSSES = 3, 3  # Amount of wins and losses that are needed for victory.
cEXCHANGES = 3         # Amount of exchanges which are performed on pairs of thimbles.


# GLOBAL VARIABLES
# ~ Yeah, I'm too lazy to design good architecture properly :)
thimble_group = ThimbleGroup()  # Let's keep all thimbles in one global group.
ball = None                     # Variable for a ball.
last_thimble_with_ball = None   # Used to play a lift animation when we're moving the ball.

animations_list = []  # List which contains animations used in main loop. Animations in this list perform concurrently.
pending_animations_list = []    # Animations in this list will perform after all animations in animations_list.

wins = losses = 0  # Counters

blink_counter = 0  # These counter and flag are used to show or hide blinking text 'Click to continue'.
show_text = True

click_step = 0     # These ones used to determine current stage of the game.
clicking_lock = False


# ENUMERATIONS
ClickStage = ienum("INITIAL", "WAITING_FOR_CHOICE", "RESETTING", "END_OF_GAME")


# MAIN SECTION
def get_two_random_thimbles():
    """Returns a tuple of two randomly selected Thimble objects. Takes ThimbleGroup object."""

    thimbles = thimble_group.sprites()
    thimble1 = thimbles[random.randint(0, 2)]
    thimble2 = thimbles[random.randint(0, 2)]
    while thimble2 is thimble1:
        thimble2 = thimbles[random.randint(0, 2)]
    return thimble1, thimble2


def click_action():
    """This function contains logic to change current stage of the game. Takes and returns nothing."""

    global show_text, clicking_lock, click_step, ball, last_thimble_with_ball, wins, losses

    # The clicking_lock flag are used to prevent performing multiple clicks before previous actions have finished.
    if clicking_lock:
        return

    # Stage 1: The initial state
    if click_step == ClickStage.INITIAL:
        show_text = False
        clicking_lock = True
        click_step += 1

        thimble1, thimble2 = get_two_random_thimbles()
        animations_list.append((thimble1.lift, [None]))  # Lift a thimble.
        if last_thimble_with_ball and last_thimble_with_ball is not thimble1:
            animations_list.append((last_thimble_with_ball.lift, [None]))
        pending_animations_list.append((ball.move, (thimble1.x + 15, thimble1.y + 10, 30)))

        # Append the rest animations to pending_animations_list
        # See sprites.py to know how to use animations and how to create your own.
        # Note that we must pass even a single parameter using a tuple or a list.
        pending_animations_list.append((thimble1.put, [None]))
        if last_thimble_with_ball and last_thimble_with_ball is not thimble1:
            pending_animations_list.append((last_thimble_with_ball.put, [None]))
        pending_animations_list.append((thimble1.pause, [10]))
        pending_animations_list.append((thimble1.exchange_with, (thimble2, None, None, 0.5)))

        thimble1.has_ball = True  # One of the thimble must "contain" the ball.
        last_thimble_with_ball = thimble1

        # Mix the thimbles
        for i in range(cEXCHANGES):
            thimble1, thimble2 = get_two_random_thimbles()
            pending_animations_list.append((thimble1.exchange_with, (thimble2, None, None, 0.5)))

    # Stage 2: The thimbles are mixed and we're waiting for user's choice
    elif click_step == ClickStage.WAITING_FOR_CHOICE:
        # Let's verify the user have clicked over one of the thimbles.
        for thimble in thimble_group.sprites():
            if thimble.mouse_over:
                break
        else:
            return

        click_step += 1
        clicking_lock = True

        for thimble in thimble_group.sprites():
            thimble.enabled = False  # Disable all thimbles to prevent performing up/down animations.

            # Lifts the chosen thimble and increments the appropriate counter.
            if thimble.mouse_over:
                animations_list.append((thimble.lift, (None, True)))

                if thimble.has_ball:
                    wins += 1
                else:
                    losses += 1

            # Moves the ball to the thimble that "contains" it before it will be shown to the user.
            if thimble.has_ball:
                ball.move_to(thimble.x + 15, thimble.y + 30)
                ball.loops_to_hide = -1
                pending_animations_list.append((thimble.lift, (None, True)))

    # Stage 3: Puts all thimbles down, enables them and resets their has_ball flags.
    elif click_step == ClickStage.RESETTING:
        clicking_lock = True

        for thimble in thimble_group.sprites():
            thimble.enabled = True
            thimble.has_ball = False

            if len(animations_list):
                pending_animations_list.append((thimble.put, [None]))
            else:
                animations_list.append((thimble.put, [None]))

        # Next stage is the initial stage again.
        show_text = True
        click_step = ClickStage.INITIAL

    # Stage 4: Player will be here when he wins or loses.
    elif click_step == ClickStage.END_OF_GAME:
        wins = losses = 0
        click_step = ClickStage.RESETTING
        click_action()


def event_loop(events):
    """This function rules the control flow depending on user's actions.
    It provides quiting the program, processing user clicks and mouse motion.
    Takes events list and returns nothing."""

    for event in events:
        if (event.type == QUIT) or (event.type == KEYDOWN and event.key == K_ESCAPE):
            sys.exit(0)
        elif event.type is MOUSEBUTTONUP and event.button is 1:
            click_action()  # This function has been described above.
        elif event.type is MOUSEMOTION:
            for thimble in thimble_group.sprites():
                # If the cursor is over a thimble and there isn't any performing animation at the same time, then we
                # should up the thimble and change the cursor
                # Don't forget to be sure that no one of animations is performing right now!
                if thimble.mouse_over and not thimble.hovered and thimble.enabled and not thimble.performing_animation:
                    animations_list.append((thimble.up, [None]))
                    pygame.mouse.set_cursor(*pygame.cursors.broken_x)
                # When the cursor moves out of the borders of the thimble, we should reset the initial states of the
                # thimble and the cursor.
                elif not thimble.mouse_over and thimble.hovered and not thimble.performing_animation:
                    animations_list.append((thimble.down, [None]))
                    pygame.mouse.set_cursor(*pygame.cursors.arrow)


def main():
    """Main function of the game. It provides creating all of the objects and running a main loop.
    Takes and returns nothing."""

    global ball, animations_list, pending_animations_list, show_text, blink_counter, clicking_lock, click_step, \
        wins, losses

    main_window = PygameWindow("Thimbles", (700, 600))  # Creates a window. See pygamext.py for more information.
    screen = main_window.screen
    bk = main_window.draw_background(image="chair.jpg")  # Set the background.

    # Creates Thimble objects and adds them to ThimbleGroup object. See sprites.py for more information.
    for x in (130, 330, 530):
        thimble_group.add(Thimble(main_window, "thimble.png", (x, 250), factor=0.25, glowing=False))

    # Creates Ball object. See sprites.py for more information.
    ball = Ball(main_window, "ball.png", (240, 400), factor=0.5, glowing=False)

    # Inits the pygame font system.
    pygame.font.init()
    font = pygame.font.SysFont(None, 20)
    bigfont = pygame.font.SysFont(None, 50)

    # Main loop
    while True:
        event_loop(pygame.event.get())  # Processes user's events. Function event_loop() has been described above.

        # Draws the background and text.
        screen.blit(bk, (0, 0))

        screen.blit(
            font.render(u"Побед: %d" % wins, True, Color("white")),
            (10, 10)
        )
        screen.blit(
            font.render(u"Поражений: %d" % losses, True, Color("white")),
            (10, 30)
        )

        # Draws the blinking text if it's needed
        blink_counter += 1
        if show_text and blink_counter < 50:
            screen.blit(
                font.render(u"Кликни, чтобы начать", True, Color("black")),
                (280, 500)
            )
        if blink_counter >= 100:
            blink_counter = 0

        # I think, the animation system is one of the most important parts of this file!
        # It uses functions whose definitions you can find in appropriate classes.
        # The main loop passes through the animations_list.
        # Each element of this list is a tuple, which contains a reference to a function, and a list or tuple of
        # arguments. The latter passes to the former.
        # Each animation function in specific classes has documentation, in which you can find a proper
        # signature, you need.
        # Next, each function returns either the same tuple with a reference to self and other arguments or None.
        # A new animation is inserted after the current animation and then the latter is removed. None means that
        # animation is finished.
        for anim in animations_list:
            next_anim = anim[0](*anim[1])
            if next_anim is not None:
                animations_list.append(next_anim)
            animations_list.remove(anim)

        # Pending animations perform only when all of animations in animations_list is finished.
        if (not len(animations_list)) and len(pending_animations_list):
            animations_list.append(pending_animations_list.pop(0))

        # If we don't have any animations, let the user to perform clicks again.
        if not len(animations_list) and not len(pending_animations_list):
            clicking_lock = False

        # Draws the ball and the thimbles by calling draw() method of these objects.
        ball.draw()
        thimble_group.draw()

        # Check conditions that determines the end of the game.
        if wins >= cWINS:
            click_step = 3
            screen.blit(
                bigfont.render(u"Поздравляю с победой!", True, Color("black")),
                (150, 350)
            )
        elif losses >= cLOSSES:
            click_step = 3
            screen.blit(
                bigfont.render(u"Сожалею, но Вы проиграли.", True, Color("black")),
                (120, 350)
            )

        # Update the screen and wait a little bit to limit the frame rate
        pygame.display.update()
        pygame.time.wait(10)


# A standard boilerplate for an executable module
if __name__ == "__main__":
    main()
