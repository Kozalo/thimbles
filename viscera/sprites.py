# -*- coding: utf-8 -*-

"""This module provides concrete sprite classes.
Currently, it contains Ball, Thimble and ThimbleGroup classes. They represent objects that are used in the game.
"""

# IMPORTS
import math
import random
import pygame
from .pygamext import KozSprite


class Ball(KozSprite):
    """A class which represents a ball on the screen. It only adds a move animation to the base class."""

    def move(self, x, y, hide_at_end=-1):
        """Move animation. Takes x and y coordinates (not in a tuple!).
        Also can take hide_at_end parameter that will cause of hiding the ball in specific amount of frames (see
        documentation of KozPySprite class for more information).
        By agreement, returns either a tuple of a reference to self and a list of parameters, or None."""

        if self.performing_animation is None:
            self.performing_animation = "move"

        new_x = int(self.x + (x - self.x) / 10)
        new_y = int(self.y + (y - self.y) / 10)

        # If this condition is true, then the animation is finished
        if new_x == self.x and new_y == self.y:
            self.performing_animation = None
            self.loops_to_hide = hide_at_end
            return None

        self.move_to(new_x, new_y)

        return self.move, (x, y, hide_at_end)


class Thimble(KozSprite):
    """A class which represents a thimble on the screen. In comparison with the base class, it has some new fields and
    methods. New fields:
    * hovered, enabled — flags; they are used in animations to prevent performing some animations concurrently,
    * lifted — a flag; used in up/down animations to prevent a case when already lifted thimble lifts again,
    * up_down_frame — a number; used in up/down animations to provide correct switches between them,
    * has_ball — a flag; True if the thimble has a ball, or False otherwise,
    * original_image — a copy of an image of the thimble; used to perform rotations in lift/put animations properly.
    """

    def __init__(self, window, img, coords, size=None, factor=None, smooth=True, glowing=True):
        """See documentation of this method of the base class."""

        super(Thimble, self).__init__(window, img, coords, size, factor, smooth, glowing)
        self.original_image = self.image.copy()

        self.hovered = False
        self.enabled = True
        self.has_ball = False
        self.lifted = False
        self.lifted_to_right = None
        self.up_down_frame = 0

    # An excellent example of the animation! Let me show you how to create animations for my animation system.
    def exchange_with(self, another, args=None, clockwise=None, ellipse_coeff=1.0):
        """A circular-motion animation of changing positions of two thimbles. The first moves to the position of the
        second one, and vice versa.
        Takes a reference to another thimble. Also you can determine direction of the animation (clockwise=True for
        clockwise, or False for counterclockwise) and set the shape of an ellipse (for example, ellipse_coeff=1.0 for
        circle, 0.5 for horizontal ellipse, 1.5 for vertical ellipse).
        If any of previous two parameters is None (by default), then its value sets randomly.
        By agreement, returns either a tuple of a reference to self and a list of parameters, or None."""

        # If we've just started the animation, let's calculate the initial values for needed parameters.
        if args is None:
            diff_x = (another.x - self.x) / 2
            diff_y = abs(diff_x) * ellipse_coeff
            start_x_pos = self.x + diff_x
            start_y_pos = self.y
            angle = 0.0

            if clockwise is None:
                clockwise = bool(random.randint(0, 1))

            self.performing_animation = "exchange_with"  # Marks what animation we started to display.

        # Otherwise, we just get necessary parameters from the "args" dictionary.
        else:
            diff_x = args["diff_x"]
            diff_y = args["diff_y"]
            start_x_pos, start_y_pos = args["start_x_pos"], args["start_y_pos"]
            angle = args["angle"]

        # We need to calculate intermediates positions...
        new_x = start_x_pos + diff_x * math.cos(angle)
        new_y = start_y_pos + diff_y * math.sin(angle)

        new_x2 = start_x_pos + diff_x * math.cos(angle + math.pi)
        new_y2 = start_y_pos + diff_y * math.sin(angle + math.pi)

        # ...and move the thimbles to them.
        another.move_to(new_x, new_y)
        self.move_to(new_x2, new_y2)

        # If the thimbles have finished their paths, reset performing_animation variable and return None.
        if angle >= math.pi or angle <= -math.pi:
            self.performing_animation = None
            return None

        # Otherwise, return a reference to this function and a list with a dictionary of new parameters.
        else:
            return self.exchange_with, (another, {
                "angle": angle + 0.05 if clockwise else angle - 0.05,
                "diff_x": diff_x,
                "diff_y": diff_y,
                "start_x_pos": start_x_pos,
                "start_y_pos": start_y_pos
            }, clockwise)

    def up(self, args=None):
        """An animation of moving up of this thimble.
        Takes nothing. By agreement, returns either a tuple of a reference to self and a list of parameters, or None."""

        if self.performing_animation is "down":
            return None

        if self.up_down_frame >= 10:
            self.performing_animation = None
            self.hovered = True
            return None

        self.performing_animation = "up"
        self.up_down_frame += 1
        self.move_to(self.x, self.y - 1)

        return self.up, [None]

    def down(self, args=None):
        """An animation of moving down of this thimble.
        Takes nothing. By agreement, returns either a tuple of a reference to self and a list of parameters, or None."""

        if self.performing_animation is "up":
            return None

        if self.up_down_frame <= 0:
            self.performing_animation = None
            self.hovered = False
            return None

        self.performing_animation = "down"
        self.up_down_frame -= 1
        self.move_to(self.x, self.y + 1)

        return self.down, [None]

    def lift(self, args=None, to_right=None):
        """An animation of lifting of this thimble.
        You can determine the direction of lifting by the to_right flag. Otherwise, the direction sets randomly.
        By agreement, returns either a tuple of a reference to self and a list of parameters, or None."""

        if self.lifted:
            return

        if args is None:
            if to_right is None:
                to_right = bool(random.randint(0, 1))

            self.performing_animation = "lift"
            angle = 0.0
            diff_x = 50

            start_x_pos = (self.x + diff_x) if to_right else (self.x - diff_x)
            start_y_pos = self.y
        else:
            diff_x = args["diff_x"]
            start_x_pos, start_y_pos = args["start_x_pos"], args["start_y_pos"]
            angle = args["angle"]

        if to_right:
            new_x = start_x_pos - diff_x * math.cos(angle)
            new_y = start_y_pos - diff_x * math.sin(angle)
        else:
            new_x = start_x_pos - diff_x * math.cos(angle + math.pi)
            new_y = start_y_pos - diff_x * math.sin(angle + math.pi)

        self.image = pygame.transform.rotate(self.original_image, -angle * 10)  # Rotates the image
        self.move_to(new_x, new_y)

        if angle >= math.pi / 2 or angle <= -math.pi / 2:
            self.enabled = False
            self.performing_animation = None
            self.lifted = True
            self.lifted_to_right = to_right
            return None
        else:
            return self.lift, ({
                                   "angle": (angle + 0.05) if to_right else (angle - 0.05),
                                   "diff_x": diff_x,
                                   "start_x_pos": start_x_pos,
                                   "start_y_pos": start_y_pos
                               }, to_right)

    def put(self, args=None):
        """An animation of putting down of this thimble. Takes nothing.
        By agreement, returns either a tuple of a reference to self and a list of parameters, or None."""

        if not self.lifted:
            return

        if args is None:
            self.performing_animation = "put"
            angle = math.pi / 2 if self.lifted_to_right else -math.pi / 2
            diff_y = 50

            start_x_pos = self.x
            start_y_pos = self.y + diff_y
        else:
            diff_y = args["diff_y"]
            start_x_pos, start_y_pos = args["start_x_pos"], args["start_y_pos"]
            angle = args["angle"]

        if self.lifted_to_right:
            new_x = start_x_pos - diff_y * math.cos(angle)
            new_y = start_y_pos - diff_y * math.sin(angle)
        else:
            new_x = start_x_pos - diff_y * math.cos(angle + math.pi)
            new_y = start_y_pos - diff_y * math.sin(angle + math.pi)

        self.image = pygame.transform.rotate(self.original_image, -angle * 10)
        self.move_to(new_x, new_y)

        if (angle >= 0 and not self.lifted_to_right) or (angle <= 0 and self.lifted_to_right):
            self.enabled = True
            self.performing_animation = None
            self.lifted = False
            return None
        else:
            return self.put, [{
                "angle": (angle - 0.05) if self.lifted_to_right else (angle + 0.05),
                "diff_y": diff_y,
                "start_x_pos": start_x_pos,
                "start_y_pos": start_y_pos
            }]


class ThimbleGroup(pygame.sprite.Group):
    """You can keep thimbles in an object of this class.
    There is only one little distinction between this class and its base class. ThimbleGroup overrides draw() method
    with no parameters."""

    def __init__(self, *sprites):
        super(ThimbleGroup, self).__init__(*sprites)

    def draw(self):
        for sprite in self.sprites():
            sprite.draw()
