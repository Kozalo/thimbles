# -*- coding: utf-8 -*-

"""This module provides a window class and extends the pygame.sprite.Sprite class to make it more functional.

Use the PygameWindow class to create a window of the application.
Use the KozSprite class to create simple objects in your project, or you can inherit a new class based on this one
to even more expand its functionality.
"""

# IMPORTS
import os
import pygame
from pygame.locals import *


class PygameWindow:
    """This class provides functions for creating a pygame window and performing some basic operations.
    Fields:
    * window — a reference to the main window created by pygame,
    * screen — a reference to the main surface for drawing,
    * title — a title of the window,
    * size — a tuple of width and height of the window,
    * images_folder — the name of a folder in which you store images."""

    def __init__(self, title, size, images_folder="images"):
        """Constructor which creates a window. Takes a title and size (width, height) for the window. Also you can
        change the name of a folder for images."""

        pygame.init()
        self.window = pygame.display.set_mode(size)
        pygame.display.set_caption(title)
        self.screen = pygame.display.get_surface()
        self.title = title
        self.size = size
        self.images_folder = images_folder

    def load_image(self, name, transparent=False):
        """Loads an image and returns it. Takes the name of the image. If the image has an alpha channel, set
        transparent to True."""

        fullname = os.path.join(self.images_folder, name)
        try:
            image = pygame.image.load(fullname)
        except pygame.error as message:
            print("Couldn't load image: %s" % name)
            raise SystemExit(message)

        image = image.convert_alpha() if transparent else image.convert()
        return image

    @staticmethod
    def resize_image(img, size, smooth=True):
        """Resizes the image to given size (width, height) and returns the result. If the "smooth" parameter is set to
        False, the result will be worse, but in this case the function works faster."""

        if smooth:
            return pygame.transform.smoothscale(img, size)
        else:
            return pygame.transform.scale(img, size)

    def scale_image(self, img, factor, smooth=True):
        """Resizes the image by a given scale factor and returns the result. If the "smooth" parameter is set to False,
        the result will be worse, but in this case the function works faster."""

        rect = img.get_rect()

        width = rect[2] - rect[0]
        height = rect[3] - rect[1]
        new_width = int(width * factor)
        new_height = int(height * factor)

        return self.resize_image(img, (new_width, new_height), smooth)

    def draw_background(self, color=None, image=None):
        """Draws an image or a color to the background.
        Takes either "image" or "color" as a parameter. If both are passed, the former takes precedence.
        Returns a new surface which contains the background."""

        screen = pygame.display.get_surface()

        background_image, background_color = None, None
        if image:
            background_image = self.load_image(image)
            background_image = pygame.transform.smoothscale(background_image, self.size)
            screen.blit(background_image, (0, 0))
        elif color:
            background_color = pygame.Surface(screen.get_size())
            background_color = background_color.convert(screen)
            background_color.fill(color)
            screen.blit(background_color, (0, 0))
        else:
            raise ValueError("Neither image or color passed!")

        pygame.display.update()
        return background_image or background_color


class KozSprite(pygame.sprite.Sprite):
    """Basic class for sprites used in the game. Adds some new generic functionality to pygame.sprite.Sprite.
    Fields:
    * window — a reference to the main window that used to draw the sprite on the screen,
    * x and y — coordinates of the sprite on the screen,
    * image — a surface with a loaded image,
    * glowing — by default, when the cursor is over the sprite, a yellow ellipse appears behind the latter; if you
    implements hovering mechanism by your own, you should set glowing to False,
    * loops_to_hide — provides a way to hide the sprite after specific amount of frames
    (0 - sprite is hidden, >0 - the value decrements per each interaction, <0 - sprite is visible),
    * performing_animation — either the name of the currently performing animation or None; used to prevent performing
    some animations concurrently; usually you must not modify this field manually!"""

    def __init__(self, window, img, coords, size=None, factor=None, smooth=True, glowing=True):
        """Constructor sets parameters of the sprite. You must pass to it:
        — [window] a reference to a PygameWindow object;
        — [img] the name of an image file (it must be stored in the 'image' folder or another folder specified by
        PygameWindow.images_folder variable);
        — [coords] coordinates of the sprite on the screen (a tuple of x and y).
        Also you can pass optional parameters:
        — [size=None] a tuple of width and height, which represents the size of the sprite, that you need (takes
        precedence on 'factor' parameter);
        - [factor=None] the size of the sprite multiplies by this parameter;
        - [smooth=True] if set the 'smooth' parameter to False, the result of resizing or scaling will be worse, but in
        this case the function works faster;
        - [glowing=True] by default, when the cursor is over the sprite, a yellow ellipse appears behind the latter;
        if you implements hovering mechanism by your own, you should pass glowing=False to the constructor of the base
        class."""

        super(KozSprite, self).__init__()
        self.window = window
        self.x, self.y = coords
        self.image = window.load_image(img, True)
        self.glowing = glowing

        if size is not None:
            self.image = window.resize_image(self.image, size, smooth)
        elif factor is not None:
            self.image = window.scale_image(self.image, factor, smooth)

        self.performing_animation = None
        self.loops_to_hide = -1

    def draw(self):
        """Provides drawing the sprite on the screen. Takes and returns nothing."""

        if self.glowing and self.mouse_over:
            newsurf = pygame.Surface(self.image.get_size())
            newsurf.set_colorkey(Color(0, 0, 0))
            pygame.draw.ellipse(newsurf, Color(255, 255, 0), (-2, -2,
                                                              self.image.get_width() + 4, self.image.get_height() + 4))
            self.window.screen.blit(newsurf, (self.x, self.y))

        if self.loops_to_hide == 0:
            return
        elif self.loops_to_hide > 0:
            self.loops_to_hide -= 1

        self.window.screen.blit(self.image, (self.x, self.y))

    def move_to(self, x, y):
        """Sets self.x and self.y to x and y respectively. Takes coordinates (not in a tuple!), returns nothing."""
        self.x, self.y = x, y

    @property
    def mouse_over(self):
        """Is the cursor over this sprite? This function checks this and returns True or False. Takes nothing."""

        x, y = pygame.mouse.get_pos()
        rect = {
            "x1": self.x,
            "y1": self.y,
            "x2": self.x + self.image.get_width(),
            "y2": self.y + self.image.get_height()
        }

        return (rect["x1"] <= x <= rect["x2"]) and \
               (rect["y1"] <= y <= rect["y2"])

    # An example of a simple animation.
    # According to comments in _game.pyw, this function must return either a reference to self and a list of new
    # parameters or None, if the animation is finished.
    def pause(self, milliseconds):
        """A simple dumb animation that does nothing and just allows to insert delays between other animations."""

        if milliseconds:
            pygame.time.wait(1)
            return self.pause, [milliseconds - 1]
        else:
            return None
