"""Just another module for utility functions."""


def enum(**enums):
    """Enumeration generator.
    https://stackoverflow.com/a/1695250"""

    return type('Enum', (), enums)


def ienum(*values):
    """Generator of integer based enumeration."""

    kv = {}
    for i, val in enumerate(values):
        kv[val] = i
    return enum(**kv)
