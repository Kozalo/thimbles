Thimbles
========

Just a simple game in Python that uses the _pygame_ module.
It's compatible with **Python 3** and **Python 2** as well, but requires the module **pygame** to be installed.  

Try to install the dependency with **pip**:

    pip install -r requirements.txt

For **Ubuntu**/**Debian** you can use the standard package manager:

    sudo apt-get install python-pygame

When dependencies will be resolved, just run the `launcher.pyw` file to launch the game.

Note that **text** in the game has been written **in Russian**!
But, nevertheless, all comments in the **code** are **in English**. 


Напёрстки
=========

Просто небольшая игра, написанная на Python с использованием модуля _pygame_.
Она совместима как с **Python 3**, так и с **Python 2**, но требует модуль **pygame**.

Чтобы установить его, попробуйте использовать **pip**:

    pip install -r requirements.txt
    
Для **Ubuntu**/**Debian** можно использовать стандартный менеджер пакетов:

    sudo apt-get install python-pygame

Когда зависимости будут разрешены, просто запустите файл `launcher.pyw`.


Screenshot / Скриншот
---------------------

![2015-10-15 23-34-46 Thimbles.png](https://bitbucket.org/repo/XX5ELzK/images/2668746500-2015-10-15%2023-34-46%20Thimbles.png)